#include "stdafx.h"
#include "windows.h"
#include <cstdio>
#include <string>
#include "MyForm.h"
#include "ComplexS.h"


using namespace System;
using namespace System::Windows::Forms;
namespace ComplexS{
	System::Void MyForm::button1_Click(System::Object^  sender, System::EventArgs^  e)
	{
		System::String^ fcn = comboBox1->Text;
		System::String^ ogr1 = textBox1->Text;
		System::String^ ogr2 = textBox2->Text;
		System::String^ ogr3 = textBox3->Text;
		int a = int::Parse(comboBox2->Text);
		int b = int::Parse(comboBox3->Text);
		int c = int::Parse(textBox4->Text);
		int d = int::Parse(textBox5->Text);
		int g = int::Parse(textBox6->Text);
		int f = int::Parse(textBox7->Text);
		int iter = int::Parse(textBox9->Text);
		double eps = double::Parse(textBox8->Text);
		make(a,b,c,d,g,f,eps,iter,fcn,richTextBox1,ogr1,ogr2,ogr3);
		this->pictureBox1->ImageLocation = "test.png";
	}
	System::Void MyForm::button3_Click(System::Object^  sender, System::EventArgs^  e)
	{
		MakeChart();
		this->pictureBox1->ImageLocation = "test.png";
	}
}
[STAThread]
void Main(array<String^>^ args)
{
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	ComplexS::MyForm myForm;        
	Application::Run(%myForm);

}