#ifndef ComplexS_h
#define ComplexS_h
#include "stdafx.h"
#include <msclr/marshal_cppstd.h>
#include "exprtk.hpp"
#include "C:\ComplexS\ComplexS\chartdir.h"
#include <iostream>
#include <cstdio>
#include <string>
#include <cmath>
#define max_iter 1000 //maxymalna liczba iteracji
#define max_zmiennych 5 
#define max_wierzcholkow 4
#define max_proby 3

template < typename T >
int sprawdz_ograniczenia(double **tab);
double promien_kola(double **tab);
void losuj(double **tab);
int znajdz_max(double **tab);
void centroid(double **tab);
void dopisz(double **tab, int iter);
int complex(double **tab);
void wypisz(int iter, System::Windows::Forms::RichTextBox^  richTextBox);
void MakeChart();
int make(int a, int b, int c, int d, int e, int f, double epsi, int iter, System::String^ fcn, System::Windows::Forms::RichTextBox^  richTextBox, System::String^ ogr1, System::String^ ogr2, System::String^ ogr3);
#endif