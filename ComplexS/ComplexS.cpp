#pragma once
#include "stdafx.h"
#include <vector>
#include <fstream>
#include "ComplexS.h"

using namespace std;

int iteracje = max_iter;
int k = 3;			//liczba wierzcholkow
int zmienne = 2;	//liczba zmienncyh
double eps = 0.001;
int liczba_ograniczen = 2;
double ograniczenia[max_zmiennych * 2] = { -10, 10, -10, 10, -10, 10, -10, 10, -10, 10 };
string tablica_ograniczen[20];
vector <double> cax1;
vector <double> cax2;
vector <double> res;
vector <double> ptx;
vector <double> pty;
int xx=0;
int cc = 0;
int all = 3;

string funkcja;
double alfa = 0.95;		//0.666  0.91
double wtab[max_iter][max_wierzcholkow][max_zmiennych];
template < typename T>

int sprawdz_ograniczenia(double **tab, int kt)
{
	cax1.clear();
	cax2.clear();
	res.clear();
	xx = 0;
	cc = 0;
	typedef exprtk::symbol_table<T> symbol_table_t;
	typedef exprtk::expression<T>     expression_t;
	typedef exprtk::parser<T>             parser_t;
	int zlicz = 0;

	for (int j = 0; j <kt; j++)			// wierzcholki
	{
		T x1 = 0, x2 = 0, x3 = 0, x4 = 0, x5, y = 0;
		symbol_table_t symbol_table;

		if (zmienne > 0)
		{
			x1 = T(tab[j][0]);
			symbol_table.add_variable("x1", x1);
		}
		if (zmienne > 1)
		{
			x2 = T(tab[j][1]);
			symbol_table.add_variable("x2", x2);
		}
		if (zmienne > 2)
		{
			x3 = T(tab[j][2]);
			symbol_table.add_variable("x3", x3);
		}
		if (zmienne > 3)
		{
			x4 = T(tab[j][3]);
			symbol_table.add_variable("x4", x4);
		}
		if (zmienne > 4)
		{
			x5 = T(tab[j][4]);
			symbol_table.add_variable("x5", x5);
		}
		string expression_funkcja = funkcja;
		symbol_table.add_constants();
		expression_t expression;
		expression.register_symbol_table(symbol_table);
		parser_t parser;
		parser.compile(expression_funkcja, expression);
		for (x1 = T(ograniczenia[0]); x1 <= T(ograniczenia[1]); x1 += T(0.25))
		{
			for (x2 = T(ograniczenia[2]); x2 <= T(ograniczenia[3]); x2 += T(0.25)){
				cax1.push_back(x1);
				cax2.push_back(x2);
				res.push_back(expression.value());
			}
		}
		x1 = T(tab[j][0]);
		x2 = T(tab[j][1]);
		y = expression.value();//wartosc funkcji w wiercholku j
		tab[j][zmienne] = y;
		symbol_table.add_variable("y", y);
	
		for (int i = 0; i <liczba_ograniczen; i++)
		{
			string expression_string = tablica_ograniczen[i];

			symbol_table.add_constants();
			expression_t expression;
			expression.register_symbol_table(symbol_table);

			parser.compile(expression_string, expression);
			T wynik = expression.value();
			if (wynik > 0)
			{
				zlicz++;
			}
			else
			{
				return 308000;		//ograniczenie funkcyjne
			}
		}

		for (int i = 0; i < zmienne; i++)
		{
			if (tab[j][i] >= ograniczenia[2 * i])
			{
				if (tab[j][i] <= ograniczenia[2 * i + 1])
				{

					zlicz = zlicz + 2;
				}
				else
				{
					return 2 * i + 1;	//ograniczenie 
				}
			}
			else
			{
				return 2 * i;
			}
		}
	}
	return 666666;	//szesc sz�stek, jest gites
}


double promien_kola(double **tab)
{
	long double *dlugosc = new long double[k];	//tablica dla "k" odcinkow
	long double suma;
	long double polowa = 0, obwod = 0;
	long double Pole = 0, R = 0;
	long double a = 1, abc = 1;
	for (int i = 0; i < k; i++)
	{
		suma = 0;
		for (int j = 0; j < zmienne; j++)
		{
			suma = suma + pow((tab[i][j] - tab[(i + 1) % k][j]), 2);	//suma (x(i,j)-x(i,j+1))^2  wierzcholki 0-1; 1-2 ;2-0
		}
		if (suma >= DBL_MAX || suma <= DBL_MIN || suma <= 0)
		{
			suma = 0.000001;
		}
		dlugosc[i] = sqrt(suma);	//DLUGOSC WEKTORA
		obwod = obwod + dlugosc[i];	//obwod trojkata
	}
	polowa = obwod / 2;
	for (int i = 0; i < k; i++)
	{
		abc = abc*dlugosc[i];	//iloczyn dlugosci odcinkow
		a = a*(polowa - dlugosc[i]);	//iloczyn (polowa obwodu - odcinek(i))
	}
	Pole = sqrt(polowa*a);	// wyliczenie pola ze wzoru herona P=sqrt(p(p-a)(p-b)(p-c))
	if (Pole <= 0)
	{
		Pole = 0.000000000001;

	}
	R = abc / (4 * Pole);	//promien okregu opisanego na trojkacie R= abc/4P
	delete[] dlugosc;
	return R;
}

void losuj(double **tab)
{
	srand(time(NULL));
	bool przekroczenie = 0;
	int flaga1 = 1;
	int kt = 1;
	int flaga2 = 0;
	while (flaga1 != 666666)
	{
		for (int i = 0; i < k; i++)				//po wierzcholkach
		{
			do
			{
				for (int j = 0; j < zmienne; j++)	//po kolejnych zmiennych
				{
					tab[i][j] = double(ograniczenia[j * 2] + double(double(rand() % 1000) / 1000) *(double(abs(double(ograniczenia[j * 2 + 1] - ograniczenia[j * 2])))));
					cout << tab[i][j] << endl;
				}
				flaga2 = int(sprawdz_ograniczenia<double>(tab, kt));
			} while (flaga2 != 666666);
			kt++;
		}
		flaga1 = int(sprawdz_ograniczenia<double>(tab, all));
	}

	if (flaga1 == 666666)
	{
		for (int i = 0; i < k; i++)
		{
			for (int j = 0; j < zmienne + 1; j++)
			{
				wtab[0][i][j] = tab[i][j];	//przepisanie  do g��wnej tablicy dla 0 iteracji
			}
		}
	}
}

int znajdz_max(double **tab)
{
	double a = -1000000000;
	int ktory = -1;
	for (int i = 0; i < k; i++)
	{
		if (tab[i][zmienne] >= a)		//tab[i][zmienne]  jest wartoscia funkcji 
		{
			a = tab[i][2];
			ktory = i;
		}
	}
	return ktory;
}


void centroid(double **tab)
{
	double a = -10000000;
	int ktory = -1;
	double *c = new double[zmienne + 1];
	int flaga = -99999;
	for (int i = 0; i < zmienne + 1; i++)
	{
		c[i] = 0;
	}
	double **pom = new double *[k];		//k -wierzcholki
	for (int i = 0; i < k; i++)
	{
		pom[i] = new double[zmienne + 1];		// x1,x2,x3,x4,x5 - zmienne (tab[0:zmienne)  y- wynik funkcji, za zmiennymi tab[zmienne+1)
	}

	for (int i = 0; i < k; i++)
	{
		for (int j = 0; j < zmienne + 1; j++)
		{
			pom[i][j] = tab[i][j];
		}
	}

	ktory = znajdz_max(tab);

	for (int i = 0; i < k; i++)
	{
		if (i != ktory)
		{
			for (int j = 0; j < zmienne; j++)
			{
				c[j] = (c[j] + tab[i][j]);		//liczenie cetroidu
			}
		}
	}
	for (int i = 0; i < zmienne; i++)
	{
		c[i] = c[i] / (k - 1);

	}

	//////////////////Wykonanie odbicia /////////////
	for (int i = 0; i < zmienne; i++)
	{
		pom[ktory][i] = c[i] * (1 + alfa) - (alfa)*tab[ktory][i];

	}

	///////////// Sprawdzenie spe�nienia ogranicze� po odbiciu///////////////
	flaga = int(sprawdz_ograniczenia<double>(pom, all));
	while (flaga != 666666)
	{
		if (flaga == 308000)		//odbicie centroidu o polowe
		{
			for (int i = 0; i < zmienne; i++)
			{
				pom[ktory][i] = c[i] - (c[i] - pom[ktory][i]) / 2;	//x*=c-(c-x*)/2
			}
		}
		else
		{
			int ogr307 = flaga;
			int calkowita = ogr307 / 2;
			int mod = ogr307 % 2;
			pom[ktory][calkowita] = ograniczenia[ogr307];	//zmiana wsp�rzednych wierzcholka na graniczna wartosc

		}
		flaga = int(sprawdz_ograniczenia<double>(pom, all));
	}

	////////////// punkt 8  //////////////////////
	int nowy = -1;
	int flaga2 = 1;
	int status = 0;
	int licznik = 0;

	while (flaga2)		 
	{
		if (licznik > 100000)
		{
			cout << "ERROR";
			break;
		}
		nowy = znajdz_max(pom);
		if (pom[nowy][zmienne]>tab[ktory][zmienne])
		{
			for (int i = 0; i < zmienne; i++)
			{
				pom[ktory][i] = c[i] * (1 + alfa) - (alfa)*pom[ktory][i];
			}

			status = sprawdz_ograniczenia<double>(pom, all);
			licznik++;
			if (status != 666666)
			{
				cout << "Status: " << status << endl;
			}
			nowy = -1;
		}
		else
		{
			for (int i = 0; i < k; i++)
			{
				for (int j = 0; j < zmienne + 1; j++)
				{
					tab[i][j] = pom[i][j];
				}
			}
			flaga2 = 0;
		}
	}
	delete[] c;
	delete[] pom;
}

void dopisz(double **tab, int iter)
{
	for (int i = 0; i < k; i++)
	{
		for (int j = 0; j < zmienne + 1; j++)
		{
			wtab[iter][i][j] = tab[i][j];
		}
	}
}

int complex(double **tab)
{
	int iter = 0;
	long double qmin = 999999.9999999999;
	losuj(tab);
	cout.precision(40);
	while (iter < iteracje - 1 && qmin>eps)
	{
		qmin = promien_kola(tab);
		if (qmin>eps)
		{
			centroid(tab);
			iter++;
			dopisz(tab, iter);
		}
	}
	return iter;
}
//
//fstream plik1;
//fstream plik2;
//fstream plik3;

void wypisz(int iter, System::Windows::Forms::RichTextBox^  richTextBox)
{
	//plik1.open("X.txt", ios::out);
	//plik2.open("Y.txt", ios::out);
	//plik3.open("Z.txt", ios::out);
	ptx.clear();
	pty.clear();
	double suma = 0;
	for (int i = 0; i < iter; i++)
	{
		richTextBox->AppendText("Iteracja: " + (i + 1)+"\n");
		for (int j = 0; j < k; j++)
		{
			richTextBox->AppendText("Wierzcholek:" + (j + 1)+"\n");
			for (int n = 0; n < zmienne + 1; n++)
			{
				richTextBox->AppendText((n+1)+ "= " + wtab[i][j][n] + "\n");
			}
			ptx.push_back((double)wtab[i][j][0]);
			pty.push_back((double)wtab[i][j][1]);
			richTextBox->AppendText("\n");
		}
		richTextBox->AppendText("\n");
	}
	for (int i = 0; i < zmienne + 1; i++)
	{
		for (int j = 0; j < k; j++)
		{
			suma = suma + wtab[iter - 1][j][i];
		}
		richTextBox->AppendText("x" + (i + 1)+" "+(suma / k)+"\n");

		suma = 0;
		//plik1.close();
		//plik2.close();
		//plik3.close();
	}
}

int make(int a, int b, int c, int d, int e, int f, double epsi, int itera, System::String^ fcn, System::Windows::Forms::RichTextBox^  richTextBox, System::String^ ogr1, System::String^ ogr2, System::String^ ogr3){
	liczba_ograniczen = a;		//trzeba ustalic liczbe ograniczen
	zmienne = b;
	eps = epsi;
	iteracje = itera;
	string pom = msclr::interop::marshal_as<std::string>(fcn);
	string pom1 = msclr::interop::marshal_as<std::string>(ogr1);
	string pom2 = msclr::interop::marshal_as<std::string>(ogr2);
	string pom3 = msclr::interop::marshal_as<std::string>(ogr3);
	// k			-	liczba wierzcholkow;
	//zmienne		-	liczba zmiennych; ograniczenie do 5 (max_wierzcholkow)
	
	tablica_ograniczen[0] = pom1;
	tablica_ograniczen[1] = pom2;
	tablica_ograniczen[2] = pom3;
	ograniczenia[0] = c;
	ograniczenia[1] = d;
	ograniczenia[2] = e;
	ograniczenia[3] = f;
	funkcja = pom;	
	int iter = 0;
	double promien = 0;
	double **tab = new double *[k];		//k -wierzcholki
	for (int i = 0; i < k; i++)
	{
		tab[i] = new double[zmienne + 1];		// x1,x2,x3,x4,x5 - zmienne (tab[0:zmienne]  y- wynik funkcji, za zmiennymi tab[zmienne+1]
	}

	iter = complex(tab);
	wypisz(iter, richTextBox);
	MakeChart();
	delete[] tab;


	//system("pause");
	return 0;
}

void MakeChart(){
	// The x and y coordinates of the grid
	int i = 0;
	int j = 0;
	int g = 0;
	int rozA = cax1.size();
	int rozB = cax2.size();
	int rozres = res.size();
	int rozpts = ptx.size();
	double pointsx[3];
	double pointsy[3];
	for (int xx = 0; xx<3; xx++)
	{
		pointsx[xx] = ptx[cc];
		pointsy[xx] = pty[cc];
		cc++;
	}
	if (cc>rozpts-3) cc = cc - 6;
	double* dataX = NULL;
	dataX = new double[rozA];
	double* dataY = NULL;
	dataY = new double[rozB];
	double* dataZ = NULL;
	dataZ = new double[rozres];
	while (i < rozA){
		dataX[i] = cax1[i];
		i++;
	}
	while (j < rozB){
		dataY[j] = cax2[j];
		j++;
	}
	while (g < rozres){
		dataZ[g] = res[g];
		g++;
	}
	// Create a XYChart object of size 600 x 500 pixels
	XYChart *c = new XYChart(611, 500);
	//c->addTitle(funkcja, "ariali.ttf", 15);

	// Set the plotarea at (75, 40) and of size 400 x 400 pixels. Use
	// semi-transparent black (80000000) dotted lines for both horizontal and
	// vertical grid lines

	c->setPlotArea(30, 10, 550, 400, -1, -1, -1, c->dashLineColor(0x80000000,
		Chart::DotLine), -1);


	// When auto-scaling, use tick spacing of 40 pixels as a guideline
	c->yAxis()->setTickDensity(40);
	c->xAxis()->setTickDensity(40);
	c->addScatterLayer(DoubleArray(pointsx, 3), DoubleArray(pointsy, 3), "", 7, 3, 0xFF0033, 0xFF0033);
	// Add a contour layer using the given data
	ContourLayer *layer = c->addContourLayer (DoubleArray(dataX, rozA), DoubleArray(dataY, rozB), DoubleArray(dataZ, rozres));

	// Move the grid lines in front of the contour layer
	//c->getPlotArea()->moveGridBefore(layer);

	// Output the chart
	c->makeChart("test.png");

	//free up resources
	delete c;
}
//void ReDraw()
//{
//
//}